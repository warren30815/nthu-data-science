from bs4 import BeautifulSoup
import requests
import numpy as np

INPUT_FILE_PATH = 'input_hw1.txt'
OUTPUT_FILE_PATH = '108062516_hw1_output.txt'
ITER_STEPS = 4

PARTS_OF_URL = ["https://www.blockchain.com/eth/address/", "?view=standard"]
PERSON_ATTR_KEYS = ['from_hash', 'Nonce', 'Number of Transactions', 'Final Balance', 'Total Sent', 'Total Received',
                    'Total Fees']

SPLIT_LINE = "-" * 74 + "\n"

ATTR_VALUE_CSS_CLASS_NAME = "sc-1ryi78w-0 bFGdFC sc-16b9dsl-1 iIOvXh u3ufsr-0 gXDEBk"
HASH_CSS_CLASS_NAME = "sc-1r996ns-0 dEMBMB sc-1tbyx6t-1 gzmhhS iklhnl-0 dVkJbV"
BLOCK_CSS_CLASS_NAME = "sc-1fp9csv-0 gkLWFf"
TRANSFER_CSS_CLASS_NAME = "sc-1ryi78w-0 bFGdFC sc-16b9dsl-1 iIOvXh u3ufsr-0 gXDEBk sc-85fclk-0 gskKpd"

def conv_hash2url(hash_str):
    return PARTS_OF_URL[0] + hash_str + PARTS_OF_URL[1]

def get_person_attrs(soup, tag_name, class_name):
    attrs = dict()
    num_of_attrs = len(PERSON_ATTR_KEYS)
    tags = soup.find_all(tag_name, class_name)
    texts = np.array([tag.get_text() for tag in tags])[:num_of_attrs]
    for i, key in enumerate(PERSON_ATTR_KEYS): 
        attrs[key] = texts[i]
    return attrs

def get_last_trans_attrs(soup, tag_name, class_name):
    attrs = {}
    attrs['Date'] = attrs['To'] = attrs['Amount'] = None
    blocks = soup.find_all(tag_name, class_name)
    for block in reversed(blocks):  # want to get the oldest transfer hash, so reverse the order of the blocks
        amount_tag = block.find("span", TRANSFER_CSS_CLASS_NAME)
        if amount_tag is not None:  # the type of the amount is "transfer", so break and return
            attrs['Date'] = block.find("span", ATTR_VALUE_CSS_CLASS_NAME).get_text()
            attrs['To'] = block.find_all("a", HASH_CSS_CLASS_NAME)[-1].get_text()  # "to_hash" is the last element of the array
            attrs['Amount'] = amount_tag.get_text()
            break
    return attrs

def log_output(file_path, log_str):
    with open(file_path, "a", encoding="utf-8") as fp:
        fp.write(log_str)

def conv_results2str(show_attrs):
    results_str = str()
    for key in show_attrs:
        value = show_attrs[key]
        if value:  # only when value is not None, it needs to be shown
            results_str += '{}: {}\n'.format(key, value)
    results_str += SPLIT_LINE
    return results_str

class EthereumCrawler():
    def __init__(self, steps):
        self.input_path = INPUT_FILE_PATH  
        self.output_path = OUTPUT_FILE_PATH
        self.steps = steps
        self.urls = []

    def get_input_urls(self, input_path):
        urls = self.urls
        with open(self.input_path, 'r') as fp:
            for each_hash in fp.readlines():
                urls.append(conv_hash2url(each_hash.strip()))  # convert hash to url

    def run(self):
        self.get_input_urls(self.input_path)

        urls = self.urls
        output_path = self.output_path
        steps = self.steps

        for url in urls:
            hashes_log = []
            next_page_url = url
            for _ in range(steps):
                r = requests.get(next_page_url)
                html_str = r.text
                soup = BeautifulSoup(html_str, features="html.parser")

                person_attrs = get_person_attrs(soup, "span", ATTR_VALUE_CSS_CLASS_NAME)
                from_hash = person_attrs['from_hash']
                hashes_log.append(from_hash)
                del person_attrs['from_hash']  # "from_hash" is not used in output.txt, so delete it

                last_trans_attrs = get_last_trans_attrs(soup, "div", BLOCK_CSS_CLASS_NAME)
                to_hash = last_trans_attrs['To']
                results_str = conv_results2str({**person_attrs, **last_trans_attrs})  # merge two dict and pass to the function
                log_output(output_path, results_str)

                if to_hash is None:  # if true means no next, break and log to output_hw1.txt
                    break
                next_page_url = conv_hash2url(to_hash)

            hashes_str = ' -> '.join(hashes_log) + "\n" + SPLIT_LINE
            log_output(output_path, hashes_str)

if __name__ == '__main__':
    crawler = EthereumCrawler(steps = ITER_STEPS)
    crawler.run()